
create sequence if not exists person_seq start 1;

CREATE TABLE if not exists person
(
    person_id   bigint      not null default nextval('person_seq'::regclass),
    first_name  varchar(256)                                   not null,
    last_name   varchar(256)                                   not null,
    middle_name varchar(256)                                   not null,
    birth_date  date,
    gender      varchar(100)                                   not null
);

alter table person
    add constraint person_pk primary key (person_id);

comment on table person is 'Инфорамция о человеке';

comment on column person.person_id is 'идентификатор.';

comment on column person.first_name is 'Имя';

comment on column person.last_name is 'Фамилия';

comment on column person.middle_name is 'Фамилия';

comment on column person.birth_date is 'Дата рождения';

comment on column person.gender is 'Пол';