package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.PersonDTO;

public interface PersonService {
    void savePerson(PersonDTO personDTO);
}
