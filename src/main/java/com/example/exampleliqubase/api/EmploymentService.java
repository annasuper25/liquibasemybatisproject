package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.EmploymentDTO;

import java.util.List;

public interface EmploymentService {
    void saveEmployment(EmploymentDTO employmentDTO);

    void updateRecords(List<EmploymentDTO> employmentDTO, Long personId);
}
