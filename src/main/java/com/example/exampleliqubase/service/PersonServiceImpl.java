package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.PersonService;
import com.example.exampleliqubase.dao.PersonMapper;
import com.example.exampleliqubase.dto.PersonDTO;
import com.example.exampleliqubase.model.PersonEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void savePerson(PersonDTO personDTO) {
        PersonEntity personEntity = modelMapper.map(personDTO, PersonEntity.class);

        personMapper.insert(personEntity);
    }
}
