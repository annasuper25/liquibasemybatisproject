package com.example.exampleliqubase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
    * запись о трудовой деятельности.
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentEntity {
    /**
    * идентификатор.
    */
    private Long employmentId;

    /**
    * реализация оптимистической блокировки
    */
    private Integer version;

    /**
    * дата начала трудовой деятельности
    */
    private LocalDate startDt;

    /**
    * дата окончания трудовой деятельности
    */
    private LocalDate endDt;

    /**
    * тип деятельности
    */
    private Long workTypeId;

    /**
    * наименование организации.
    */
    private String organizationName;

    /**
    * адрес организации.
    */
    private String organizationAddress;

    /**
    * должность.
    */
    private String positionName;

    private Long personId;
}