package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dto.EmploymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employments")
public class EmploymentController {

    @Autowired
    private EmploymentService employmentService;

    @PostMapping("/save")
    public void saveEmployment(@RequestBody @Validated EmploymentDTO employmentDTO) {
        employmentService.saveEmployment(employmentDTO);
    }

    @PostMapping(value = "/records/update/{personId}")
    public void updateRecords(@PathVariable Long personId,
                              @RequestBody @Validated List<EmploymentDTO> employmentsDTO) {
        employmentService.updateRecords(employmentsDTO, personId);
    }

}
