package com.example.exampleliqubase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.validation.constraints.NotNull;

/**
 * Информация о человеке
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDTO {
    /**
     * идентификатор человека
     */
    private Long personId;

    /**
     * Имя
     */
    @NotNull
    private String firstName;

    /**
     * Фамилия
     */
    @NotNull
    private String lastName;

    /**
     * Фамилия
     */
    @NotNull
    private String middleName;

    /**
     * Дата рождения
     */
    private Date birthDate;

    /**
     * Пол
     */
    @NotNull
    private String gender;
}