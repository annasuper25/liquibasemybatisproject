package com.example.exampleliqubase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDTO {
    /**
     * идентификатор записи о трудовой деятельности
     */
    private Long employmentId;

    /**
     * реализация оптимистической блокировки
     */
    @NotNull
    private Integer version;

    /**
     * дата начала трудовой деятельности
     */
    private LocalDate startDt;

    /**
     * дата окончания трудовой деятельности
     */
    private LocalDate endDt;

    /**
     * тип деятельности
     */
    private Long workTypeId;

    /**
     * наименование организации
     */
    private String organizationName;

    /**
     * адрес организации
     */
    private String organizationAddress;

    /**
     * должность
     */
    private String positionName;

    /**
     * идентификатор работника
     */
    private Long personId;

}
