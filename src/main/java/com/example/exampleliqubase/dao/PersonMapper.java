package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.PersonEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectKey;

public interface PersonMapper {
    int deleteByPrimaryKey(Long personId);

    @Insert("insert into person (person_id, first_name, last_name,\n" +
            "                            middle_name, birth_date, gender)\n" +
            "        values (#{personId}, #{firstName}, #{lastName},\n" +
            "                #{middleName}, #{birthDate}, #{gender})")
    @SelectKey(statement="select nextval('person_seq')", keyProperty="personId", before=true, resultType=Long.class)
    int insert(PersonEntity record);

    int insertSelective(PersonEntity record);

    PersonEntity selectByPrimaryKey(Long personId);

    int updateByPrimaryKeySelective(PersonEntity record);

    int updateByPrimaryKey(PersonEntity record);
}