package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.EmploymentEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Set;

public interface EmploymentMapper {
    int deleteByPrimaryKey(Long employmentId);

    @Insert("insert into employment (employment_id, version, start_dt,\n" +
            " end_dt, work_type_id, organization_name,\n" +
            " organization_address, position_name, person_id) " +
            "values(#{employmentId}, #{version}, #{startDt}, #{endDt}, #{workTypeId}," +
            " #{organizationName},#{organizationAddress}, #{positionName}, #{personId})")
    @SelectKey(statement="select nextval('employment_seq')", keyProperty="employmentId", before=true, resultType=Long.class)
    int insert(EmploymentEntity record);

    @Update("update employment (employment_id, version, start_dt,\n" +
            "            \" end_dt, work_type_id, organization_name,\n" +
            "            \" organization_address, position_name, person_id) SET (#{employmentId}, #{version}, #{startDt}, #{endDt}, #{workTypeId},\" +\n" +
            "            \" #{organizationName},#{organizationAddress}, #{positionName}, #{personId}) where employment_id = #{employmentId}")
    void update(Long employmentId);

    int insertSelective(EmploymentEntity record);

    EmploymentEntity selectByPrimaryKey(Long employmentId);

    int updateByPrimaryKeySelective(EmploymentEntity record);

    int updateByPrimaryKey(EmploymentEntity record);

    @Select("select * from employment where person_id = #{personId}")
    List<EmploymentEntity> getByPersonId(Long personId);

    void insertAll(@Param("list") List<EmploymentEntity> employmentsToSave);

    void updateAll(@Param("list") List<EmploymentEntity> employmentsToUpdate);

   // @Delete("delete from employment WHERE employment_id IN  <foreach item="id" collection="ids" separator="," open="(" close=")"> #{id}  </foreach>")
    void deleteByIds(@Param("ids") Set<Long> ids);

    @Delete("delete from employment WHERE employment_id = #{id}")
    void deleteById(@Param("id") Long id);
}