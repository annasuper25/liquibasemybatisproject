create sequence if not exists employment_seq start 1;

CREATE TABLE if not exists employment
(
    employment_id        bigint not null default nextval('employment_seq'::regclass),
    version              int8                                            not null,
    start_dt             date,
    end_dt               date,
    work_type_id         bigint,
    organization_name    varchar(256),
    organization_address text,
    position_name        varchar(256),
    person_id            bigint
);

alter table employment
    add constraint employment_pk primary key (employment_id);

ALTER TABLE employment
    ADD CONSTRAINT rerson_fk FOREIGN KEY (person_id) REFERENCES person (person_id);

comment on table employment is 'запись о трудовой деятельности.';

comment on column employment.employment_id is 'идентификатор.';

comment on column employment.version is 'реализация оптимистической блокировки';

comment on column employment.start_dt is 'дата начала трудовой деятельности';

comment on column employment.end_dt is 'дата окончания трудовой деятельности';

comment on column employment.work_type_id is 'тип деятельности';

comment on column employment.organization_name is 'наименование организации.';

comment on column employment.organization_address is 'адрес организации.';